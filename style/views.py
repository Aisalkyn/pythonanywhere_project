from django.http import HttpResponse
from django.shortcuts import redirect,render
from django.urls import reverse
from django.views import View
from django.views.generic import TemplateView
from django import forms

from style.models import Feedback


class HomeView(TemplateView):
    template_name = "home.html"

class ContactsView(TemplateView):
    template_name = "contacts.html"

class ProductsView(TemplateView):
    template_name = "products.html"

class MyPageView(TemplateView):
    template_name = "my_page.html"

class RegisterView(TemplateView):
    template_name = "registration/register.html"

    def dispatch(self, request, *args, **kwargs):
        if request.method == 'POST':
            username = request.POST.get('username')
            email = request.POST.get('email')
            password = request.POST.get('password')
            password2 = request.POST.get('password2')

            if password == password2:
                from django.contrib.auth.models import User
                User.objects.create_user(username, email, password)
                return redirect(reverse("login"))

        return render(request, self.template_name)

class ProfilePage(TemplateView):
    template_name = "registration/profile.html"


class FeedbackAjaxVeiw(View):
    def dispatch(self, request, *args, **kwargs):
        name =request.GET.get("name")
        email =request.GET.get("email")
        message =request.GET.get("message")

        if name and email and message:
            f =Feedback(name=name, email=email, message =message)
            f.save()
            return HttpResponse(content="success")
        return HttpResponse(content="error")

