function show()
    {
       var per1,per2,per3,per4,result;
       per1= document.getElementById('p1').value;
       per1= parseInt(per1);

       per2= document.getElementById('p2').value;
       per2= parseInt(per2);

       per3= document.getElementById('p3').value;
       per3= parseInt(per3);

       per4= document.getElementById('p4').value;
       per4= parseInt(per4);

       result= per4+per3+per2+per1;

       document.getElementById('result').innerHTML=result;
    }

function send()
    {
        var fio;
        fio=document.getElementById('s1').value;
        alert("Уважаемый(ая), "+fio+ ", ваше сообщение принято!");
        $("#form").hide();
    }
function showForm()
    {
        $("#formButton").on("click",function () {
            $("#form").show();
        });
    }

function onTitleClick() {
   $("#feedback").toggleClass("open")
}

function onSuccess(response) {
    if(response === 'success'){
        alert("Ваше сообщение успешно отправлено!");
        $("feedback").removeClass("open");
        $("#fb-name").val("");
        $("#fb-email").val("");
        $("#fb-message").val("");
    } else {
        alert("Попробуйте еще раз!")
    }
}

function onError() {
    alert("Попробуйте еще раз!")
}

function submitFeedback() {
    var name = $("#fb-name").val();
    var email = $("#fb-email").val();
    var message = $("#fb-message").val();


    $.ajax("/fb/", {
        data:{
            name: name,
            email: email,
            message: message
        },
        success: onSuccess,
        error: onError
    });
    return false;
}

function onReady() {
    $("#feedback .title").on("click",onTitleClick)

    $("#submit-feedback").on("click",submitFeedback);

}
$(document).ready(onReady);

