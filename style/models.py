from django.db import models

class Subscriber(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()
    mobile = models.CharField(max_length=100,verbose_name=u"Mobile number", null=True,blank=True)

    def __str__(self):
        return self.name

class Feedback(models.Model):
    name = models.CharField(max_length=255, verbose_name= u"Имя")
    email = models.EmailField(verbose_name= u"Email")
    message = models.TextField(verbose_name= u"Сообщение")